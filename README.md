# Tabletop RPG Bot for Discord

A bot that does all the math for you, all you need to do is tell it!

If you wish to contribute, feel free to submit a pull/merge request.

## Game support

Supported games:
* Dungeons & Dragons 5th Edition

Games planned to be supported:
* Call of Cthulhu
* Dungeons & Dragons 3rd Edition
* Pathfinder 1st Edition
* Shadowrun 5th Edition

## How do I deploy it?
TODO