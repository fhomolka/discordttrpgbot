#!/usr/bin/env python

import json
import discord
import random
import datetime
import os
#Import local files
import modules.DnD5e as game
import base

random.seed(datetime.datetime.now())

#Open the token file
with open('TOKEN') as token_file:
    TOKEN = token_file.read().replace('\n', '')
    token_file.close()

#and Change the name of the game we're currently playing
gameName ="Tabletop RPG"

#Shortcut a bit
client = discord.Client()

def is_me(message):
    return message.author == client.user

def is_command(message):
    return message.content.startswith('!')

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    #turn the message into lowercase, makes it easier on ifs
    #message.content = message.content.lower() #.replace(' ', '') #might need this later

    if is_command:

        #Change the name of the Game
        if message.content.startswith('!chgame'):
            gameName = message.content.replace("!chgame ", '')
            msg = "Changed game to " + gameName
            await client.change_presence(game=discord.Game(name=gameName))
            await message.channel.send(msg)

        #Delete previous 100 messages and commands
        if message.content.startswith('!clear'):
            deleted = await message.channel.purge(limit=100, check=is_me)
            deleted += await message.channel.purge(limit=100, check=is_command)
            msg ='Deleted {} message(s)'.format(len(deleted))  
            await message.channel.send(msg)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")
            deleted = await message.channel.purge(limit=100, check=is_me)

        #Roll the dice
        if message.content.startswith('!roll'):
            message.content = message.content.replace("!roll", '').replace(' ', '')
            msg = base.roll_the_dice(message)
            await message.channel.send(msg)

        #Check Rolls
        if message.content.startswith('!skill'):
            msg = game.skill_check(message)
            await message.channel.send(msg)
        
        #Check Saving Throws
        if message.content.startswith('!save'):
            msg = game.save_throw(message)
            await message.channel.send(msg)

        #Add players
        if message.content.startswith('!play'):
            msg = game.add_player(message)
            await message.channel.send(msg)

        #Remove Plaers
        if message.content.startswith('!leave'):
            msg = game.remove_player(message)
            await message.channel.send(msg)


        #Print out the character Sheet
        if message.content.startswith('!mysheet'):
            msg = game.print_sheet(message)
            for embed in msg:
                await message.channel.send(embed=embed)


@client.event
async def on_ready():
    await client.change_presence(activity=discord.Game(name=gameName))
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

os.system('clear')
client.run(TOKEN)
