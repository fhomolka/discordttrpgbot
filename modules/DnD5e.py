#Module: DnD5e
#All things related to Dungeons & Dragons 5th Edition go here

import json
import datetime
import random
import discord

#import local files
import base

random.seed(datetime.datetime.now())

#Prepairing variables for later use
#We can add inf players
players = []

#Fuck adding an entire dict, let's make a json for modifiers
with open('skill_modifiers.json') as modifiers:
    skill_modifier = json.load(modifiers)
    modifiers.close()

#Add the user to the active player list
def add_player(message):
    message.content = (message.content.replace("!play", '')).replace(' ', '')
    fixedAuthor = message.author.mention.replace('!', '')

    #Check whether the player is already in the game
    for player in players:
        if fixedAuthor == player["playerID"]:
            return "{0.author.mention} you are already playing!".format(message)
    
    #Add the player to the list of players
    with open('players/DnD5e/{0}.json'.format(fixedAuthor)) as sheet:
        players.append(json.load(sheet))
        sheet.close()
 
    msg = "Added {0.author.mention} to the game.".format(message)
    return msg

#Remove the user from the active player list
def remove_player(message):
    message.content = (message.content.replace("!leave", '')).replace(' ', '')
    fixedAuthor = message.author.mention.replace('!', '')
    
    #Check whether the player is actually in the game
    msg = "{0.author.mention} , you're not even playing!".format(message)
    for player in players:
        if fixedAuthor == player["playerID"]:
            players.remove(player)
            msg = "Removed {0.author.mention} from the game.".format(message)
            break
    
    return msg

#Load the specific player Character Sheet, roll and add any modifiers
def skill_check(message):
    message.content = (message.content.replace("!skill", '')).replace(' ', '')
    if message.content == '':
        return "{0.author.mention} Usage: !skill <name of the skill>".format(message)
    message.content = message.content.lower()

    #Find the player in the list
    currentPlayer = {}
    for player in players:
        if message.author.mention == player["playerID"]:
            currentPlayer = player
        pass
    #remember which skill we actually need
    currentSkill = message.content 
    message.content = "1d20"
    roll = base.diceRoll(message)

    #Get the result, add proficiency only if needed
    try:
        result = roll[1] + currentPlayer["modifiers"][skill_modifier[currentSkill]]
        if currentPlayer["skills"][currentSkill]:
            result += currentPlayer["proficiency"]

        return "{0.author.mention} rolled {1} = {2}".format(message, currentSkill, result)
    except KeyError:
        return"{0.author.mention}, that is not a skill".format(message)

def save_throw(message):
    message.content = (message.content.replace("!save", '')).replace(' ', '')
    if message.content == '':
        return "{0.author.mention} Usage: !save <short name of attribute>".format(message)
    message.content = message.content.lower()

    #Find the player in the list
    currentPlayer = 0
    for player in players:
        if message.author.mention == player["playerID"]:
            currentPlayer = player
        pass
    #remember which skill we actually need
    currentSave = message.content 
    message.content = "1d20"
    roll = base.diceRoll(message)

    #Get the result, add proficiency only if needed
    result = roll[1] + player["modifiers"][currentSave]
    if player["saves"][currentSave]:
        result += int(player["proficiency"])

    return "{0.author.mention} rolled {1} = {2}".format(message, currentSave, result)


# display the character sheet
def print_sheet(message):
    message.content = (message.content.replace("!mysheet", '')).replace(' ', '')
    fixedAuthor = message.author.mention.replace('!', '')

    #Find the player in the list
    currentPlayer = 0
    for player in players:
        if fixedAuthor== player["playerID"]:
            currentPlayer = player
        pass

    pack = []

    embed0=discord.Embed(title=currentPlayer["characterName"], description="Attributes", color=0x033d77)
    for key, val in currentPlayer["attributes"].items():
        embed0.add_field(name=key, value=val, inline=True)
    pack.append(embed0)
    embed1=discord.Embed(title=currentPlayer["characterName"], description="Saving Throws", color=0x033d77)
    for key, val in currentPlayer["saves"].items():
        embed1.add_field(name=key, value=val, inline=True) 
    pack.append(embed1) 
    embed2=discord.Embed(title=currentPlayer["characterName"], description="Skills", color=0x033d77)
    for key, val in currentPlayer["skills"].items():
        embed2.add_field(name=key, value=val, inline=True) 
    
    pack.append(embed2)

    return pack

