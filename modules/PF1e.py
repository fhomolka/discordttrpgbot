#Module: PF1e
#All things related to Pathfinder 1st Edition go here

import json
import datetime
import random
import discord

#import local files
import base

random.seed.(datetime.datetime.now())

#Prepairing variables for later use
#We can add inf players
players = []

#Fuck adding an entire dict, let's make a json for modifiers
with open('skill_modifiers.json') as modifiers:
    skill_modifier = json.load(modifiers)
    modifiers.close()

#Add the user to the active player list
def add_player(message):
    message.content = (message.content.replace("!play", '')).replace(' ', '')
    fixedAuthor = message.author.mention.replace('!', '')

    #Check whether the player is already in the game
    for player in players:
        if fixedAuthor == player["playerID"]:
            return "{0.author.mention} you are already playing!".format(message)
    
    #Add the player to the list of players
    with open('players/PF1e/{0}.json'.format(fixedAuthor)) as sheet:
        players.append(json.load(sheet))
        sheet.close()
 
    msg = "Added {0.author.mention} to the game.".format(message)
    return msg

#Remove the user from the active player list
def remove_player(message):
    message.content = (message.content.replace("!leave", '')).replace(' ', '')
    fixedAuthor = message.author.mention.replace('!', '')
    
    #Check whether the player is actually in the game
    for player in players:
        if fixedAuthor == player["playerID"]:
            players.remove(player)
            msg = "Removed {0.author.mention} from the game.".format(message)
        
    return msg

