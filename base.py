import random
import datetime


random.seed(datetime.datetime.now())


def roll_the_dice(message):
    if message.content == '' or 'd' not in message.content:
        return "{0.author.mention} Usage: !roll <number of dice>d<how many sides>".format(message)
    message.content = message.content.lower()
    dice = message.content.split("d", 1)
    if '+' in dice[1]:
        dice[1] = dice[1].split('+', 1)
        dice.append(dice[1][1])
        dice[1] = dice[1][0]
    print(dice)
    if dice[0] == '':
        dice[0] = '1'
    result = 0
    individualRolls = []
    for x in range(0, int(dice[0])):
        currentRoll = random.randint(1, int(dice[1]))
        result += int(currentRoll)
        individualRolls.append(currentRoll)
        pass
    if len(dice) > 2:
        result += int(dice[2])
        msg = "{0.author.mention} rolled ".format(message) + str(dice[0]) + "d" + str(dice[1]) + " + " + str(dice[2]) + " = " + str(result) + "``` " + str(individualRolls)+"```"
    else:
        msg = "{0.author.mention} rolled ".format(message) + str(dice[0]) + "d" + str(dice[1]) + " = " + str(result) + " ```" + str(individualRolls)+"```"
    return msg


def diceRoll(message):
    message.content = message.content.lower()
    dice = message.content.split("d", 1)
    if '+' in dice[1]:
        dice[1] = dice[1].split('+', 1)
        dice.append(dice[1][1])
        dice[1] = dice[1][0]
    print(dice)
    if dice[0] == '':
        dice[0] = '1'
    result = 0
    individualRolls = []
    for x in range(0, int(dice[0])):
        currentRoll = random.randint(1, int(dice[1]))
        result += int(currentRoll)
        individualRolls.append(currentRoll)
        pass
    if len(dice) > 2:
        result += int(dice[2])
    pack = [dice, result, individualRolls]
    return pack
   